from conans import ConanFile, MSBuild, tools


class CppaclibConan(ConanFile):
    requires = [("jsoncpp/1.0.0@theirix/stable"), ("libcurl/7.70.0")]
    default_options = {"openssl:no_asm": "True", "openssl:shared": "False", "libcurl:shared": "False", "jsoncpp:shared": "False"}
    name = "cppaclib"
    version = "1.1"
    license = "MIT"
    author = "Sergey Sergeev <sersoftin@gmail.com>"
    url = "https://bitbucket.org/sersoftin/cppaclib"
    description = "Cpp access control library client"
    settings = "os", "compiler", "build_type", "arch"
    generators = "visual_studio"
    exports_sources = "CppACLib/*", "*.sln", "conan/conanbuildinfo.props"

    def build(self):
        msbuild = MSBuild(self)
        msbuild.build("CppACLib.sln")


    def package(self):
        self.copy("*.h", dst="include", src="CppACLib")
        self.copy("*.lib", dst="lib", keep_path=False)


    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
