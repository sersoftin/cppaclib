#include "Tower.h"

namespace AccessControlLibrary
{
	namespace Hardware
	{
		Tower::Tower(std::string hash_salt)
			:_hash_salt(hash_salt)
		{
			_towerInfo = new TowerSmbiosInfo();
		}

		std::string Tower::getUniqueKey() const
		{
			std::ofstream fout("link_system.log", std::ios::app);
			auto baseboard = _towerInfo->getBaseboard();
			fout << "baseboard.manufacturer: " << baseboard.getManufacturer() << std::endl;
			fout << "baseboard.product: " << baseboard.getProduct() << std::endl;
			fout << "baseboard.serial_number: " << baseboard.getSerialNumber() << std::endl;
			auto system_enclosure = _towerInfo->getSystemEnclosure();
			fout << "system_enclosure.manufacturer: " << system_enclosure.getManufacturer() << std::endl;
			fout << "system_enclosure.serial_number: " << system_enclosure.getSerialNumber() << std::endl;
			auto baseboardHash = _towerInfo->getBaseboard().getHash();
			fout << "baseboardHash: " << baseboardHash << std::endl;
			auto systemEnclosureHash = _towerInfo->getSystemEnclosure().getHash();
			fout << "systemEnclosureHash: " << systemEnclosureHash << std::endl;
			auto uuidHash = Util::sha256(_towerInfo->getPcUuid());
			fout << "uuid: " << _towerInfo->getPcUuid() << std::endl;
			fout << "uuidHash: " << uuidHash << std::endl;
			fout.close();
			return Util::sha256(baseboardHash + ":" + systemEnclosureHash + ":" + uuidHash + ":" + _hash_salt);
		}

		std::string Tower::getPcName() const
		{
			return getenv("COMPUTERNAME");
		}

		Tower::~Tower()
		{
			delete _towerInfo;
		}
	}
}
