#include "TowerSmbiosInfo.h"

namespace AccessControlLibrary
{
	namespace Hardware
	{
		TowerSmbiosInfo::TowerSmbiosInfo()
		{
			const BYTE bytes_signature[] = { 'B', 'M', 'S', 'R' };
			const DWORD signature = *((DWORD*)bytes_signature);
			DWORD need_buffer_size = GetSystemFirmwareTable(signature, 0, nullptr, 0);
			auto _smbios_buffer = LPBYTE(malloc(need_buffer_size));
			auto retVal = GetSystemFirmwareTable(signature, 0, _smbios_buffer, need_buffer_size);
			if (!retVal)
			{
				char message[512];
				sprintf(message, "Unknown error on GetSystemFirmwareTable. retVal: %d. Error code: %d.", retVal, GetLastError());
				throw Exceptions::SmbiosException(message);
			}
			if (retVal > need_buffer_size)
			{
				char message[512];
				sprintf(message, "Need more memory for output buffer. Allocated: %d bytes, need: %d bytes.", retVal, need_buffer_size);
				throw Exceptions::SmbiosException(message);
			}
			const PRawSMBIOSData pDMIData = (PRawSMBIOSData)_smbios_buffer;
			dumpSmbiosStructure(&(pDMIData->SMBIOSTableData), pDMIData->Length);
			if (_smbios_buffer)
				free(_smbios_buffer);
		}

		std::string TowerSmbiosInfo::getPcUuid() const
		{
			return _pc_uuid;
		}

		Baseboard TowerSmbiosInfo::getBaseboard() const
		{
			return _baseboard;
		}

		SystemEnclosure TowerSmbiosInfo::getSystemEnclosure() const
		{
			return _system_enclosure;
		}

		TowerSmbiosInfo::~TowerSmbiosInfo()
		{
		}

		void TowerSmbiosInfo::dumpSmbiosStructure(void *Addr, UINT Len)
		{
			LPBYTE p = (LPBYTE)(Addr);
			const DWORD lastAddress = ((DWORD)p) + Len;
			PSMBIOSHEADER pHeader;

			for (;;) {
				pHeader = (PSMBIOSHEADER)p;
				dispatchStructureType(pHeader);
				PBYTE nt = p + pHeader->Length;
				while (0 != (*nt | *(nt + 1))) nt++;
				nt += 2;
				if ((DWORD)nt >= lastAddress)
					break;
				p = nt;
			}
		}

		void TowerSmbiosInfo::dispatchStructureType(PSMBIOSHEADER hdr)
		{
			switch (hdr->Type)
			{
			case 1:
			{
				processSystemInformation(hdr);
				break;
			}
			case 2:
			{
				processBaseboardInfo(hdr);
				break;
			}
			case 3:
			{
				processSystemEnclosure(hdr);
				break;
			}
			}
		}

		void TowerSmbiosInfo::processSystemInformation(void* p)
		{
			auto pSystem = PSystemInfo(p);
			auto uuid = new char[69];
			ZeroMemory(uuid, 69);
			if (pSystem->Header.Length < 0x1B)
			{
				sprintf(uuid, "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X",
					pSystem->UUID[0], pSystem->UUID[1], pSystem->UUID[2], pSystem->UUID[3],
					pSystem->UUID[4], pSystem->UUID[5], pSystem->UUID[6], pSystem->UUID[7],
					pSystem->UUID[8], pSystem->UUID[9], pSystem->UUID[10], pSystem->UUID[11],
					pSystem->UUID[12], pSystem->UUID[13], pSystem->UUID[14], pSystem->UUID[15]);
			}
			else
			{
				sprintf(uuid, "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X",
					pSystem->UUID[3], pSystem->UUID[2], pSystem->UUID[1], pSystem->UUID[0],
					pSystem->UUID[5], pSystem->UUID[4], pSystem->UUID[7], pSystem->UUID[6],
					pSystem->UUID[8], pSystem->UUID[9], pSystem->UUID[10], pSystem->UUID[11],
					pSystem->UUID[12], pSystem->UUID[13], pSystem->UUID[14], pSystem->UUID[15]);
			}

			_pc_uuid = std::string(uuid);
			delete uuid;
		}

		void TowerSmbiosInfo::processSystemEnclosure(void* p)
		{
			auto pSysEnclosure = PSystemEnclosure(p);
			auto str = SmbiosStringUtil::toPointString(p);
			_system_enclosure.setManufacturer(std::string(SmbiosStringUtil::locateStringA(str, pSysEnclosure->Manufacturer)));
			_system_enclosure.setSerialNumber(std::string(SmbiosStringUtil::locateStringA(str, pSysEnclosure->SN)));
		}

		void TowerSmbiosInfo::processBaseboardInfo(void* p)
		{
			auto pBoard = PBoardInfo(p);
			auto str = SmbiosStringUtil::toPointString(p);
			_baseboard.setManufacturer(std::string(SmbiosStringUtil::locateStringA(str, pBoard->Manufacturer)));
			_baseboard.setProduct(std::string(SmbiosStringUtil::locateStringA(str, pBoard->Product)));
			_baseboard.setSerialNumber(std::string(SmbiosStringUtil::locateStringA(str, pBoard->SN)));
		}
	}
}