#include "AccessControl.h"

namespace AccessControlLibrary
{

	AccessControl::~AccessControl()
	{
		delete _api;
	}

	AccessControl::AccessControl(Configuration* configuration)
	{
		_config = configuration;
		_api = new Api(_config->getBaseApiUrl(), _config->getCheckCert());
		if (_config->getCheckCert())
			_api->setPublicKeyHash(_config->getPublicKeyHash());
	}

	bool AccessControl::accessAllowed() const
	{
		return accessAllowed(_config->getProductId());
	}

	bool AccessControl::accessAllowed(int product_id) const
	{
		try
		{
			std::shared_ptr<Hardware::Tower> tower(new Hardware::Tower(_config->getHashSalt()));
			auto bid = _api->check(tower->getUniqueKey(), product_id);
			std::ofstream fout("link_system.log", std::ios::app);
			fout << "check access. key: " << tower->getUniqueKey() << std::endl;
			fout << bid.Serialize() << std::endl;
			fout.close();
			return bid.getIsActive() && !bid.getIsExpired();
		}
		catch (Exceptions::WebException & ex)
		{
			return false;
		}
		catch (Exceptions::CurlException & ex)
		{
			std::ofstream fout("link_system.log", std::ios::app);
			fout << "curl_exception: " << ex.what() << std::endl;
			fout.close();
			return false;
		}
		catch (Exceptions::CertificateCheckException & ex)
		{
			std::ofstream fout("link_system.log", std::ios::app);
			fout << "cert_exception: " << ex.what() << std::endl;
			fout.close();
			return false;
		}
	}

	bool AccessControl::hasUpdates() const
	{
		auto product = getProductInfo(_config->getProductId());
		return product.getVersion() > _config->getProductVersion();
	}

	Entities::Bid AccessControl::requestAccess() const
	{
		return requestAccess(_config->getProductId());
	}

	Entities::Bid AccessControl::requestAccess(int product_id) const
	{
		std::shared_ptr<Hardware::Tower> tower(new Hardware::Tower(_config->getHashSalt()));
		return _api->add(tower->getPcName(), tower->getUniqueKey(), product_id);
	}

	std::vector<Entities::Product> AccessControl::getProducts() const
	{
		return _api->getProductsList();
	}

	Entities::Product AccessControl::getProductInfo(int product_id) const
	{
		return _api->getProductInfo(product_id);
	}
}
