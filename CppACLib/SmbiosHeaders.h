#pragma once


namespace AccessControlLibrary
{
	namespace Hardware
	{
#pragma pack(push) 
#pragma pack(1)
		typedef struct _RawSMBIOSData
		{
			BYTE Used20CallingMethod;
			BYTE SMBIOSMajorVersion;
			BYTE SMBIOSMinorVersion;
			BYTE DmiRevision;
			DWORD Length;
			PBYTE SMBIOSTableData;
		} RawSMBIOSData, *PRawSMBIOSData;

		typedef struct _SMBIOSHEADER_
		{
			BYTE Type;
			BYTE Length;
			WORD Handle;
		} SMBIOSHEADER, *PSMBIOSHEADER;

		typedef struct _TYPE_1_
		{
			SMBIOSHEADER Header;
			UCHAR Manufacturer;
			UCHAR ProductName;
			UCHAR Version;
			UCHAR SN;
			UCHAR UUID[16];
			UCHAR WakeUpType;
			UCHAR SKUNumber;
			UCHAR Family;
		} SystemInfo, *PSystemInfo;

		typedef struct _TYPE_2_
		{
			SMBIOSHEADER Header;
			UCHAR Manufacturer;
			UCHAR Product;
			UCHAR Version;
			UCHAR SN;
			UCHAR AssetTag;
			UCHAR FeatureFlags;
			UCHAR LocationInChassis;
			UINT16 ChassisHandle;
			UCHAR Type;
			UCHAR NumObjHandle;
			UINT16* pObjHandle;
		} BoardInfo, *PBoardInfo;

		typedef struct _TYPE_3_
		{
			SMBIOSHEADER Header;
			UCHAR Manufacturer;
			UCHAR Type;
			UCHAR Version;
			UCHAR SN;
			UCHAR AssetTag;
			UCHAR BootupState;
			UCHAR PowerSupplyState;
			UCHAR ThermalState;
			UCHAR SecurityStatus;
			ULONG32 OEMDefine;
			UCHAR Height;
			UCHAR NumPowerCord;
			UCHAR ElementCount;
			UCHAR ElementRecordLength;
			UCHAR pElements;
		} SystemEnclosureInfo, *PSystemEnclosure;
#pragma pack(push)
	}
}
