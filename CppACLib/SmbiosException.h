#pragma once
#include <exception>
#include <string>

namespace AccessControlLibrary
{
	namespace Exceptions
	{
		class SmbiosException : public std::exception
		{
		public:
			explicit SmbiosException(char const* const message);
			explicit SmbiosException(std::string& message);
			~SmbiosException();
		};
	}
}
