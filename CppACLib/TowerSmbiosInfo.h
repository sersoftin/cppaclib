#pragma once
#include <Windows.h>
#include "SmbiosStringUtil.h"
#include "Baseboard.h"
#include "SystemEnclosure.h"
#include "SmbiosException.h"

namespace AccessControlLibrary
{
	namespace Hardware
	{
		class TowerSmbiosInfo
		{
		public:
			TowerSmbiosInfo();

			std::string getPcUuid() const;
			Baseboard getBaseboard() const;
			SystemEnclosure getSystemEnclosure() const;

			~TowerSmbiosInfo();

		private:

			Baseboard _baseboard;
			SystemEnclosure _system_enclosure;
			std::string _pc_uuid;

			void dumpSmbiosStructure(void *Addr, UINT Len);
			void dispatchStructureType(PSMBIOSHEADER hdr);
			void processSystemInformation(void* p);
			void processSystemEnclosure(void* p);
			void processBaseboardInfo(void* p);
		};

	}
}