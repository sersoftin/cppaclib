#pragma once
#include <Windows.h>
#include "SmbiosHeaders.h"

namespace AccessControlLibrary
{
	namespace Hardware
	{
		class SmbiosStringUtil
		{
		public:
			static const char * locateStringA(const char* str, UINT i);
			static const wchar_t * locateStringW(const char* str, UINT i);
			static const char* toPointString(void* p);
		};
	}
}