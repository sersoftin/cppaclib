#include "SmbiosStringUtil.h"

namespace AccessControlLibrary
{
	namespace Hardware
	{
		const char* SmbiosStringUtil::locateStringA(const char* str, UINT i)
		{
			static const char strNull[] = "";
			if (0 == i || 0 == *str) return strNull;
			while (--i)
			{
				str += strlen(const_cast<char*>(str)) + 1;
			}
			return str;
		}

		const wchar_t* SmbiosStringUtil::locateStringW(const char* str, UINT i)
		{
			static wchar_t buff[2048];
			auto pStr = locateStringA(str, i);
			SecureZeroMemory(buff, sizeof(buff));
			MultiByteToWideChar(CP_OEMCP, 0, pStr, strlen(pStr), buff, sizeof(buff));
			return buff;
		}

		const char* SmbiosStringUtil::toPointString(void* p)
		{
			return (char*)p + ((PSMBIOSHEADER)p)->Length;
		}
	}
}
