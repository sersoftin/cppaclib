#include "SmbiosException.h"

namespace AccessControlLibrary
{
	namespace Exceptions
	{
		SmbiosException::SmbiosException(char const* const message)
			: exception (message)
		{

		}

		SmbiosException::SmbiosException(std::string& message)
			: exception(message.c_str())
		{

		}

		SmbiosException::~SmbiosException()
		{

		}
	}
}